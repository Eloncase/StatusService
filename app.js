const express = require("express");
const app = express();
const router = express.Router();
const config = require("./server/config");
const pinger = require("./server/pinger");
const notify = require("./server/notify");
const bind = require("./server");

router.use(express.static("build"));
bind(router);
app.use(config.get("urn"), router);

pinger();
notify();

app.listen(config.get("port"), () => { console.log("Listening"); });