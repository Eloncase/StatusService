import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
    display: flex;
    flex-direction: row;
    align-items: center;
    height: 100px;
`;

const Link = styled.a`
    margin-right: 25px;
    font-size: 1.4em;
`;

const Span = styled.span`
    color: #444444;
    margin-right: 5px;
`;

function Header(props) {
    return (
        <Wrapper>
            <div>
                <Link href="/">Services Status</Link>
                <Span>Refreshing in</Span>
                <Span>{props.sec}</Span>
                <Span>seconds</Span>
            </div>
        </Wrapper>
    );
}

export default Header;