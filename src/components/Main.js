import React from 'react';
import styled from 'styled-components';

import Error from './Error';
import Service from './Service';

const Wrapper = styled.div`
    display: flex;
    flex-direction: column;
    flex: 1;
`;

function Main(props) {
    return (
        <Wrapper>
            <Error error={props.error} />
            <Service json={props.json} />
        </Wrapper>
    );
}

export default Main;