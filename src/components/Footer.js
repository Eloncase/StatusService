import React from 'react';
import styled from 'styled-components';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faRaspberryPi, faReact, faNodeJs, faFontAwesomeAlt } from '@fortawesome/free-brands-svg-icons';
import { faCopyright } from '@fortawesome/free-regular-svg-icons';

const Wrapper = styled.div`
    display: flex;
    flex-direction: row-reverse;
`;

const Column = styled.div`
    display: flex;
    flex-direction: column;
    align-items: end;
    color: #444444;
    margin: 10px 0;
`;

const FA = styled(FontAwesomeIcon)`
    margin-left: 4px;
`;

const A = styled.a`
    color: #444444;
`;

function Footer(props) {
    return (
        <Wrapper>
            <Column>
                <span>Powered by
                    <A href="https://raspberrypi.org/" title="Raspberry Pi"><FA icon={faRaspberryPi} /></A>
                    <A href="https://reactjs.org" title="React"><FA icon={faReact} /></A>
                    <A href="https://nodejs.org/" title="NodeJS"><FA icon={faNodeJs} /></A>
                    <A href="https://fontawesome.com/" title="Font Awesome"><FA icon={faFontAwesomeAlt} /></A>
                </span>
                <span><FA icon={faCopyright} /> Eloncase 2019</span>
            </Column>
        </Wrapper>
    );
}

export default Footer;