import React from 'react';
import styled from 'styled-components';
import moment from 'moment';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faExclamationTriangle } from '@fortawesome/free-solid-svg-icons';

moment.relativeTimeThreshold('ss', 10);

const Wrapper = styled.div`
    display: flex;
    flex-direction: column;
    border-top: 1px solid #444444;
`;

const Item = styled.div`
    margin-top: 15px;
    padding: 5px;
    display: flex;
    flex-direction: row;
    align-items: center;
`;

const ItemNameHolder = styled.div`
    flex: 5;
    display: flex;
    flex-direction: column;
`;

const ItemName = styled.a`
    text-decoration: ${props => props.href ? "underline" : "none"};
`;

const ItemInfo = styled.span`
    color: #444444;
    padding-right: 5px;
`;

const ItemStatus = styled.span`
    color: ${props => props.status === "Normal" ? "#6CBF42" : props.status === "Bad" ? "#BFB442" : "#BF4242"}
    flex: 1;
    text-align: right;
    white-space: nowrap;
`;

const FA = styled(FontAwesomeIcon)`
    margin-left: 3px;
`;

function Service(props) {
    var json = props.json || [];

    return (
        <Wrapper>
            {json.map((item) =>
                <Item key={item.id}>
                    <ItemNameHolder>
                        <ItemName href={item.url}>{item.name || item.id}</ItemName>
                        <ItemInfo>updated <span>{moment.unix(item.updated).fromNow()}</span></ItemInfo>
                        <ItemInfo>uptime <span>{item.uptime ? moment.unix(item.uptime).toNow(true) : `-`}</span></ItemInfo>
                    </ItemNameHolder>
                    <ItemInfo title="Ping">{item.ping ? `${item.ping}ms` : ``}</ItemInfo>
                    <ItemStatus title={item.error} status={item.status}>
                        {item.status}
                        {item.error &&
                            <FA icon={faExclamationTriangle} />
                        }
                    </ItemStatus>
                </Item>
            )}
        </Wrapper>
    );
}

export default Service;