import React from 'react';
import styled from 'styled-components';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faExclamationTriangle } from '@fortawesome/free-solid-svg-icons';

const Block = styled.div`
    padding: 15px;
    background-color: #ff565688;
    border-radius: 10px;
    flex-direction: row;
`;

const FA = styled(FontAwesomeIcon)`
    margin-right: 10px;
`;

function Error(props) {
    if (props.error) {
        return (
            <Block>
                <FA icon={faExclamationTriangle} />
                <span>{props.error.message}</span>
            </Block>
        );
    } else {
        return <></>;
    }
}

export default Error;