import { createGlobalStyle } from 'styled-components'

const GlobalStyle = createGlobalStyle`
    body {
        background: #323234;
        color: #aaaaaa;
        margin: 0;
        padding: 0;
        font-family: "Segoe UI", "Roboto", "Droid Sans", "Helvetica Neue", sans-serif;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
    }

    a {
        text-decoration: none;
        color: #aaaaaa;
    }

    code {
        font-family: Menlo, Monaco, Consolas, "Courier New", monospace;
    }
`

export default GlobalStyle;