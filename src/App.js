import React, { Component } from 'react';
import { isMobile } from 'react-device-detect';
import styled from 'styled-components'
import GlobalStyle from './theme/globalStyle'
import Header from './components/Header.js';
import Main from './components/Main.js';
import Footer from './components/Footer.js';

const AppWrapper = styled.div`
    display: flex;
    flex-direction: column;
    min-height: 100vh;
    width: ${props => props.mobile ? "95" : "40"}vw;
    margin: auto;
`;

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            refreshIn: 0,
            error: null,
            json: null,
        };
    }

    tick() {
        if (this.state.refreshIn > 0) {
            this.setState({
                refreshIn: this.state.refreshIn - 1
            });
        } else {
            this.clearInt();
            this.getJson();
        }
    }

    getJson() {
        fetch(window.location.href + "get")
            .then(res => res.json())
            .then(json => this.setState({ refreshIn: 60, json: json, error: null }))
            .catch(err => this.setState({ refreshIn: 60, error: err }))
            .finally(() => this.armInt());
    }

    armInt() {
        this.timerId = setInterval(() => {
            this.tick();
        }, 1000);
    }

    clearInt() {
        clearInterval(this.timerId);
    }

    componentDidMount() {
        this.getJson();
    }

    componentWillUnmount() {
        this.clearInt();
    }

    render() {
        return (
            <>
                <GlobalStyle />
                <AppWrapper mobile={isMobile}>
                    <Header sec={this.state.refreshIn} />
                    <Main error={this.state.error} json={this.state.json} />
                    <Footer />
                </AppWrapper>
            </>
        );
    }
}

export default App;
