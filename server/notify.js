﻿const moment = require("moment");
const fetch = require("node-fetch");
const si = require("systeminformation");
const store = require("./store");
const config = require("./config");

const notify = config.get("notify");

function init() {
    if (notify.onInfo && notify.infoTimeout && notify.infoTimeout > 0) {
        setTimeout(notifyInfo, notify.infoTimeout);
    }

    if (notify.onStatus) {
        store.event.on("statusChange", (prevStatus, item) => {
            notifyStatus(item, notify.onStatus);
        });
    }
}

function servicesField() {
    let services = store.get();

    let normal = 0;
    let bad = 0;
    let crit = 0;

    for (let i in services) {
        let item = services[i];
        switch (item.status) {
            case "Normal":
                normal++;
                break;
            case "Bad":
                bad++;
                break;
            case "Critical":
                crit++;
                break;
        }
    }

    return {
        name: "👾 Services",
        value: `Normal \`${normal}\`\nBad \`${bad}\`\nCritical \`${crit}\``,
        inline: true
    };
}

function generalOSField() {
    let time = si.time();

    return {
        name: "ℹ️ General",
        value: `Server time \`${moment(time.current).toISOString()} ${time.timezone}\`\nUptime \`${Math.floor(moment.duration(time.uptime, "seconds").asDays())}\`d`,
        inline: true
    };
}

async function hwField() {
    let sys = await si.system();
    let cpu = await si.cpu();
    let cpuTemp = await si.cpuTemperature();

    return {
        name: "🖥️ Hardware",
        value: `${sys.manufacturer} ${sys.model}\n` +
            `CPU \`${cpu.manufacturer} ${cpu.brand}\`\n` +
            `Temp \`${cpuTemp.main}\``,
        inline: true
    };
}

async function osField() {
    let os = await si.osInfo();
    let mem = await si.mem();
    let load = await si.currentLoad();

    return {
        name: "📊 OS Info",
        value: `${os.platform} ${os.release}\n` +
            `Mem \`${Math.floor(mem.active / 1024 / 1024)}M ${Math.floor(mem.total / 1024 / 1024)}M\`\n` +
            `Swap \`${Math.floor(mem.swapused / 1024 / 1024)}M ${Math.floor(mem.swaptotal / 1024 / 1024)}M\`\n` +
            `Avg.Load \`${load.avgload.toFixed(2)}\`\nCur.Load \`${load.currentload.toFixed(2)}\``,
        inline: true
    };
}

async function ioField() {
    let io = await si.disksIO();
    let size = await si.fsSize();

    let str = "";
    for (let i in size) {
        str += `\`${size[i].fs}\` \`${Math.floor(size[i].size / 1024 / 1024)}M\` \`${size[i].use}%\`\n`;
    }

    return {
        name: "💿 IO",
        value: str +
            `rIO \`${(io.rIO / 1024 / 1024).toFixed(2)}M\`\n` +
            `wIO \`${(io.wIO / 1024 / 1024).toFixed(2)}M\`\n` +
            `rIO/s \`${(io.rIO_sec / (io.ms / 1000) / 1024).toFixed(2)}K\`\n` +
            `wIO/s \`${(io.wIO_sec / (io.ms / 1000) / 1024).toFixed(2)}K\``,
        inline: true
    };
}

async function networkField() {
    let def = await si.networkInterfaceDefault();
    let nw = await si.networkInterfaces();
    let stats = await si.networkStats();
    let ip = await (await fetch("https://api.ipify.org/?format=text")).text();

    let nwd = nw.find(i => i.iface == def);
    let st = stats.find(i => i.iface == def);

    return {
        name: "🕸️ Network",
        value: `iface \`${def}\`\n` +
            `ip4 \`${nwd.ip4}\`\n` +
            `ext.ip \`${ip}\`\n` +
            `trx \`${(st.rx_bytes / 1024 / 1024).toFixed(2)}M\`\n` +
            `ttx \`${(st.tx_bytes / 1024 / 1024).toFixed(2)}M\`\n` +
            `rx/s \`${(st.rx_sec / (st.ms / 1000) / 1024).toFixed(2)}K\`\n` +
            `tx/s \`${(st.tx_sec / (st.ms / 1000) / 1024).toFixed(2)}K\``,
        inline: true
    };
}

async function notifyInfo() {
    console.log("Starting notify info builder");
    let fields = [];
    fields.push(servicesField());
    fields.push(generalOSField());

    let promises = [ hwField, osField, ioField, networkField ];

    for (let i in promises) {
        let pr = promises[i];
        try {
            fields.push(await pr());
        } catch (e) {
            console.log(`Couldn't add field '${pr.name}' notify info: ${e.message}`);
        }
    }

    let embed = embedBase();
    embed.fields = fields;

    console.log("Sending notify info");
    sendEmbed(embed, notify.onInfo, () => setTimeout(notifyInfo, notify.infoTimeout));
}

function notifyStatus(item, hook) {
    let embed = embedBase();
    embed.description = `\`${item.name}\`'s status has changed to \`${item.status}\``;

    switch (item.status) {
        case "Normal":
            embed.color = 7126850;
            break;
        case "Bad":
            embed.color = 12563522;
            break;
        case "Critical":
            embed.color = 12534338;
            break;
    }

    console.log("Sending notify status");
    sendEmbed(embed, hook, () => { });
}

function embedBase() {
    let embedDefaults = {
        title: "Status",
        timestamp: moment().toISOString()
    };

    return Object.assign({}, embedDefaults, notify.embed);
}

function sendEmbed(embed, hook, callback) {
    let json = {
        embeds: [embed]
    };

    fetch(hook, {
        method: "post",
        body: JSON.stringify(json),
        headers: { "Content-Type": "application/json" },
    })
    .then(() => {
        console.log("Sent embed");
    })
    .catch((e) => {
        console.log("Error while sending embed: " + e.message);
    }).finally(() => {
        callback();
    });
}

module.exports = init;