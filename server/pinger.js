const moment = require("moment");
const fetch = require("node-fetch");
const shell = require("shelljs");
const config = require("./config");
const store = require("./store");

const services = config.get("services");
const list = Object.assign({}, services);
const timeout = config.get("pingTimeout");
const random = config.get("randomizePing");

function init() {
    for (let prop in list) {
        let item = list[prop];
        let to = timeout;

        if (random && random > 0) {
            to += Math.random() * random;
        }

        if (item.pingUrl) {
            console.log("Initing pinger for " + prop);

            let pingFunc = () => {
                let ping = moment().valueOf();
                let error = null;
                console.log("Pinging " + prop);

                fetch(item.pingUrl)
                    .then(res => {
                        if (res.status != 200) {
                            throw new Error(res.status + " " + res.statusText);
                        }

                    })
                    .catch(err => {
                        error = err;
                        if (err.code == "ETIMEDOUT") ping = 0;
                    })
                    .finally(() => {
                        let result = error ? "Bad" : "Normal";
                        console.log("Ping for " + prop + ": " + result);
                        store.add(prop, result, ping == 0 ? null : moment().valueOf() - ping, error);

                        setTimeout(pingFunc, to);
                    })
            };

            pingFunc();
        } else if (item.bash) {
            console.log("Initing bash for " + prop);

            let bashFunc = () => {
                console.log("Executing bash for " + prop);
                let error = null;
                let res = shell.exec(item.bash);
                if (res.code !== 0) {
                    error = res.stderr || item.bashError
                }
                let result = error ? "Bad" : "Normal";
                console.log("Bash for " + prop + ": " + result);
                store.add(prop, result, null, error);

                setTimeout(bashFunc, to);
            };

            bashFunc();
        }
    }
}

module.exports = init;