const moment = require("moment");
const config = require("./config");
const event = require("events");

const services = config.get("services");
const list = Object.assign({}, services);
const timeout = config.get("allowSilent");
const emitter = new event();

setInterval(() => {
    for (let prop in list) {
        let item = list[prop];
        let now = moment().unix();

        if (!item.updated || (now - item.updated) * 1000 > timeout) {
            item.updated = now;
            item.error = "Missing heartbeat";
            increaseFail(item);
        }
    }
}, timeout);

function add(key, status, ping, error) {
    if (!key || !status) return;
    if (status != "Normal" && status != "Bad") status = "Critical";
    if (!list[key]) list[key] = {};

    let item = list[key];
    let prevStatus = item.status;

    item.status = status;
    if (ping) item.ping = ping;
    if (error) {
        item.error = error;
        increaseFail(item);
    } else {
        item.error = null;
        item.fail = 0;
    }

    if (item.status == "Normal" && !item.uptime) {
        item.uptime = moment().unix();
    }

    item.updated = moment().unix();

    if (prevStatus && prevStatus != item.status) {
        emitter.emit("statusChange", prevStatus, item);
    }
}

function get() {
    let res = [];

    for (let prop in list) {
        let r = {};
        let item = list[prop];

        r.id = prop;
        if (item.name) r.name = item.name;
        if (item.error) r.error = item.error;
        if (item.url) r.url = item.url;
        if (item.ping) r.ping = item.ping;
        r.updated = item.updated;
        r.status = item.status || "Critical";
        r.uptime = item.uptime || null;

        res.push(r);
    }

    return res;
}

function increaseFail(item) {
    if (!item.fail) {
        item.fail = 1;
    } else {
        if (item.fail < 10) item.fail++;
    }

    if (item.fail > 2) {
        item.status = "Critical";
        item.uptime = null;
    }

    item.error = item.error + " (" + item.fail + ")";
}

module.exports = {
    add: add,
    get: get,
    event: emitter
};