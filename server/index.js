const store = require("./store");

function get(req, res) {
    res.setHeader("Content-Type", "application/json");
    res.write(JSON.stringify(store.get()));
    res.end();
}

function post(req, res) {
    // someday
}

module.exports = (router) => {
    router.get("/get", get);
    router.post("/", post);
};