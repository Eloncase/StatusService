# Eloncase Status

## Requirments
Change `homepage` inside `package.json` if necessary. Run `npm install` in root to get all the dependencies.

## Build
After that you can build react project, in order to do that you should run `npm run build`.

## Configuration
Change `config.json` in `server/config/config.json`:
```json
{
  "port": 8084, // port which will be used by express
  "urn": "/", // if you want to run server at http://localhost/server you should set it to /server
  "secret": "absolutelysecretkey" // used for jwt
  "allowSilent": 600000, // how often fail counter will increase
  "pingTimeout": 500000, // how often ping will be emitted
  "randomizePing": 5000, // random time that will be added to pingTimeout
  "notify": {
    "onStatus": "https://discordapp.com/api/webhooks/id/key", // discord webhook link
    "onInfo": "https://discordapp.com/api/webhooks/id/key",
    "infoTimeout": 3600000, // how often info embed will be sent
    "embed": { } // embed object that will oveerride defaults
  },
  "services": {
    "some-service": { // service id
      "name": "FriendlyName" // user friendly name (optional)
      "pingUrl": "https://some-service.com/ping", // what to ping (optional)
      "url": "https://some-service.com", // redirect on click (optional)
    },
    "some-local-process": {
      "name": "AnotherFriendlyName",
      "bash": "ps -aux | grep ProcessName.dll | grep -v grep", // if code != 0 will take stderr as error output
      "bashError": "Process is not running" // if stderr is empty will take that (optional)
    }
  }
}
```

## Usage

You should be good to go. Simply run `npm run server`.